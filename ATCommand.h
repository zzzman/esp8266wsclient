  //reference http://wiki.iteadstudio.com/ESP8266_Serial_WIFI_Module
 
  const char RestartESP[]="AT+RST";
  const char SetSTAMode[]="AT+CWMODE="; //1= Sta, 2= AP, 3=both
  const char WifiConnect[]="AT+CWJAP="; //"ssid","password"
  const char GetIPAddress[]="AT+CIFSR"; //Get IP address
  const char SetConnection[]="AT+CIPMUX=";//0 for single connection 1 for multiple connection
  const char StartSingleTCPSession[]="AT+CIPSTART=\"TCP\","; //"host", port
  const char QuitAP[]="AT+CWQAP"; //quit AP
  const char SetupConnection[]="AT+CIPSTART="; // setup TCP or UDP connection
  const char SendData[]="AT+CIPSEND=";// Send data
  const char CloseConnection[]="AT+CIPCLOSE";//close TCP or UDP connection
  
  
  
