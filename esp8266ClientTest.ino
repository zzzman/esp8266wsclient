#include <SoftwareSerial.h>
#include "ESP8266HTTPClient.h"
#include "base64.h"
#include "XXTEA.h"
#include "reviseBase64ForGET.h"

ESP8266HTTPClient *esp8266;
XXTEA *xxtea;

void setup() {
   esp8266 = new  ESP8266HTTPClient(F("YOURSSID"), F("AP PASSWORD"), 2, 3, 115200);
   xxtea = new XXTEA("YOUR XXTEA PASSWORD MUST 16CHARS");

}
void loop() {
    String response = "";
    char input[128];
    char b64[256];  
    strcpy (input, "{\"d\":\"50.87\", \"k\":\"c2816177637863837653aea1f447b7f895d2111c\"}");
    xxtea->encrypt(input);  
    
    base64_encode(b64, input,strlen(input));  
    reviseBase64ForGET(b64);
    
    esp8266->sendMessage(F("YOUR SERVER IP"), YOUR SERVER PORT, "/?i=", b64, &response);
    xxtea->decrypt(input);
    Serial.println(input);
    delay(1000);
}

