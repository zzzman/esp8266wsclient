//Created by: Wyatt Huang
//A Simple http client for arduino

#include <SoftwareSerial.h>
#include <Arduino.h>

class ESP8266HTTPClient
{
  public:
      ESP8266HTTPClient(String WifiSSID, String WifiPassword, int ESPRXPin, int ESPTXPin, long BaudRate);
      ~ESP8266HTTPClient();

      boolean sendMessage(String server, int port,String path,String message, String* response);

      boolean httpGET(String *server, int *port,String *resourceURI, String* response);

  private:
      boolean httpAction(String httpVerb, String *server, int *port,String *resourceURI,String *content, String* response);
      boolean initWifi(String WifiNetworkNameSSID, String WifiPassword, long BaudRate);
      SoftwareSerial *_esp8266Serial;
};
