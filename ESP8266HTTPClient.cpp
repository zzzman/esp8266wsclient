//Created by: Wyatt Huang
//A Simple http client for arduino
#include <SoftwareSerial.h>
#include "ESP8266HTTPClient.h"
#include "ATCommand.h"

//#define _DEBUG
#define TIMEOUT 1000
#define WIFI_RETRY 100
#define ARDUINO_SERIAL_BAUD 9600

ESP8266HTTPClient::ESP8266HTTPClient(String WifiNetworkNameSSID, String WifiPassword, int ESPRXPin = 2, int ESPTXPin = 3, long BaudRate = 11520) {
   this->_esp8266Serial = new SoftwareSerial(ESPRXPin, ESPTXPin);
   boolean isWifiOK = false; 
   int retry = 0;
   while (false == isWifiOK && retry++ < WIFI_RETRY) {
       isWifiOK = initWifi(WifiNetworkNameSSID, WifiPassword, BaudRate);
       #if defined(_DEBUG)
           if (false == isWifiOK) {
               Serial.print(F("retrying "));
               Serial.println(String(retry));
           }
       #endif
       delay(50);
   }   
}

ESP8266HTTPClient::~ESP8266HTTPClient() {
    if (NULL != _esp8266Serial) {
         delete _esp8266Serial;
    }
}


boolean ESP8266HTTPClient::initWifi(String WifiSSID, String WifiPassword, long BaudRate)
{
  Serial.begin(ARDUINO_SERIAL_BAUD);
  #if defined(_DEBUG)
  Serial.begin(ARDUINO_SERIAL_BAUD);
  Serial.println(F("Debug Serial Port opened"));
  Serial.println(BaudRate);
  #endif
  boolean isESP8266Available = false;
  this->_esp8266Serial->begin(BaudRate);
  this->_esp8266Serial->setTimeout(TIMEOUT);

  delay(50);
  this->_esp8266Serial->println(F("AT"));
  delay(50);
 
  #if defined(_DEBUG)
  Serial.println(F("checking"));
  #endif

  if (this->_esp8266Serial->find("OK"))
  {
    isESP8266Available = true;
    #if defined(_DEBUG)
        Serial.println(F("ESP 8266 is available and responding..")); 
    #endif 
  } else {
    #if defined(_DEBUG)
        Serial.println(F("ESP 8266 is not available.."));
    #endif 
    return false;
  }

  this->_esp8266Serial->println(QuitAP);
  this->_esp8266Serial->print(SetSTAMode);
  this->_esp8266Serial->println("1");
  delay(50);
  
  if (this->_esp8266Serial->find("OK")) {
      #if defined(_DEBUG)
          Serial.print(SetSTAMode); 
          Serial.print(F("1")); 
          Serial.println(F(" success"));
      #endif     
  } else {
      #if defined(_DEBUG)
          Serial.print(SetSTAMode); 
          Serial.println(F(" fail"));
      #endif  
      return false;
  }
    
  this->_esp8266Serial->print(WifiConnect);
  this->_esp8266Serial->print(F("\""));
  this->_esp8266Serial->print(WifiSSID);
  this->_esp8266Serial->print(F("\""));
  this->_esp8266Serial->print(F(","));
  this->_esp8266Serial->print(F("\""));
  this->_esp8266Serial->print(WifiPassword);
  this->_esp8266Serial->println(F("\""));
  delay(50);
  
  this->_esp8266Serial->println(GetIPAddress);
  delay(50);
  /*
  if (this->_esp8266Serial->find("OK")) {
      #if defined(_DEBUG)
          Serial.print(GetIPAddress); 
          Serial.println(F(" success"));
      #endif     
  } else {
      #if defined(_DEBUG)
          Serial.print(GetIPAddress);
          Serial.println(F(" fail")); 
      #endif  
      return false;
  }
*/
  this->_esp8266Serial->print(SetConnection);
  this->_esp8266Serial->println(F("0"));
  
  return isESP8266Available;
}

boolean ESP8266HTTPClient::sendMessage(String server, int port, String resourceURI, String message, String* response)
{
  return httpAction(F("GET"), &server, &port, &resourceURI, &message, response);
}

boolean ESP8266HTTPClient::httpAction(String httpVerb, String *server, int *port, String *resourceURI, String *content, String* response) {
  
  char tcpPacket[512];
  sprintf(tcpPacket, "%s %s%s HTTP/1.1\r\nHost: %s:%d\r\n\r\n\r\n", httpVerb.c_str(), resourceURI->c_str(), content->c_str(),server->c_str(), *port);

  Serial.println(tcpPacket);


#if defined(_DEBUG)
  Serial.println(F("Opening TCP Connection.."));
  Serial.print(F("Command : "));
  Serial.print(StartSingleTCPSession);
  Serial.print(F("\""));
  Serial.print(*server);
  Serial.print(F("\""));
  Serial.print(F(","));
  Serial.println(*port);
#endif

  this->_esp8266Serial->print(StartSingleTCPSession);
  this->_esp8266Serial->print(F("\""));
  this->_esp8266Serial->print(*server);
  this->_esp8266Serial->print(F("\""));
  this->_esp8266Serial->print(F(","));
  this->_esp8266Serial->println(*port);
 
  if (this->_esp8266Serial->find("OK")) {
#if defined(_DEBUG)
    Serial.println(F("Connected.."));
    Serial.println(F("Sending Packet Length..."));
    Serial.print(F("Command : "));
    Serial.print(SendData);
    Serial.println(strlen(tcpPacket)); 
#endif
    
    this->_esp8266Serial->print(SendData);
    this->_esp8266Serial->println(strlen(tcpPacket));
    delay(50);
    Serial.println(tcpPacket);
  //  if (this->_esp8266Serial->find(">")) {
#if defined(_DEBUG)
    Serial.println(F("Sending Packet...."));
    Serial.println(tcpPacket);
#endif

    this->_esp8266Serial->println(tcpPacket);
   // }
    delay(50);
   
    if (this->_esp8266Serial->find("OK")) {
#if defined(_DEBUG)
      Serial.println(F("Packet Sent success"));
#endif
        String esp8266SerialResponse = "";
        while (this->_esp8266Serial->available() > 0) {
          delay(5);
          esp8266SerialResponse += this->_esp8266Serial->readString();
        }
      *response = esp8266SerialResponse;
      this->_esp8266Serial->println(CloseConnection);
      return true;
    } else {
#if defined(_DEBUG)
      Serial.println(F("Sending Packet Failed"));
#endif
      return false;
    }
  } else {
#if defined(_DEBUG)
    Serial.println(F("Cannot Start TCP Session"));
#endif
    this->_esp8266Serial->println(CloseConnection);
    return false;
  }
  
}


