#include "reviseBase64ForGET.h"
//replace + to -
// / to _
void reviseBase64ForGET(char* msg)
{
    while (*msg!='\0'){
        if( *msg == '+' ) {
            *msg = '-';
        } 
        if( *msg == '/' ) {
            *msg = '_';
        }   
        msg++;
    }
}
